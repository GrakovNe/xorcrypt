package org.grakovne.xorcryptor.services;

import org.grakovne.xorcryptor.common.RolesPostfixManager;
import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.exceptions.EntityAlreadyExists;
import org.grakovne.xorcryptor.exceptions.EntityNotFound;
import org.grakovne.xorcryptor.exceptions.IncorrectPassword;
import org.grakovne.xorcryptor.repositories.UserRepository;
import org.grakovne.xorcryptor.roles.UserRole;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class UserService {
    private UserRepository userRepository;

    public UserService() {
        userRepository = new UserRepository();
    }

    public User signInUser(String userName, String password) {
        User user = userRepository.getUserByName(userName);
        checkUser(user);

        if (isUser(user, password)) {
            user.setRole(UserRole.USER);
        }

        if (isAdmin(user, password)) {
            user.setRole(UserRole.ADMIN);
        }

        if (null == user.getRole()){
            throw new IncorrectPassword();
        }

        return user;
    }

    public User signUpUser(String userName, String password, UserRole userRole) {
        User user = userRepository.getUserByName(userName);

        if (null != user){
            throw new EntityAlreadyExists();
        }

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            user = new User();

            user.setUserName(userName);

            String userPassword = null;
            switch (userRole) {
                case ADMIN:
                    userPassword = password + RolesPostfixManager.getAdminRolePostfix();
                    break;
                case USER:
                    userPassword = password + RolesPostfixManager.getUserRolePostfix();
                    break;
            }

            userPassword = new String(md.digest(userPassword.getBytes()), "UTF-8");

            user.setPassword(userPassword);

            userRepository.createUser(user);

            return user;

        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        throw new IncorrectPassword();
    }

    public List<User> getAll(){
        return userRepository.getUsers();
    }

    public User getUserByUserName(String userName){
        return userRepository.getUserByName(userName);
    }

    private void checkUser(User user) {
        if (null == user) {
            throw new EntityNotFound();
        }
    }

    private boolean isUser(User user, String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            String userPassword = password + RolesPostfixManager.getUserRolePostfix();
            userPassword = new String(md.digest(userPassword.getBytes()), "UTF-8");

            return userPassword.equals(user.getPassword());
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean isAdmin(User user, String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            String userPassword = password + RolesPostfixManager.getAdminRolePostfix();
            userPassword = new String(md.digest(userPassword.getBytes()), "UTF-8");

            return userPassword.equals(user.getPassword());
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return false;
    }
}
