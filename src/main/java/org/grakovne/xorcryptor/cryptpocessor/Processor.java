package org.grakovne.xorcryptor.cryptpocessor;

import java.io.UnsupportedEncodingException;

public class Processor {

    private String originalText;
    private String keyString;

    public Processor(String originalText, String keyString) {
        setOriginalText(originalText);
        setKey(keyString);
    }

    private void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    private void setKey(String keyString) {
        this.keyString = keyString;
    }


    private String processText() {
        byte[] textArray = originalText.getBytes();
        byte[] keyArray = keyString.getBytes();

        byte[] resultArray = new byte[textArray.length];

        for (int i = 0; i < originalText.length(); i++) {
            resultArray[i] = (byte) (textArray[i] ^ keyArray[i % keyArray.length]);
        }

        try {
            return new String(resultArray, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String encodeText() {
        return processText();
    }

    public String decodeText() {
        return processText();
    }

}
