package org.grakovne.xorcryptor.cryptpocessor;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import java.io.UnsupportedEncodingException;

public class Base64Wrapper {
    public String encodeText(String text, String key) {
        Processor processor = new Processor(text, key);
        return Base64.encode(processor.encodeText().getBytes());
    }

    public String decodeText(String text, String key) {
        try {

            Processor processor = new Processor(new String(Base64.decode(text), "UTF-8"), key);
            return processor.decodeText();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
