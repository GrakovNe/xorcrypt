package org.grakovne.xorcryptor.fileprocessor;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.exceptions.FileIsNotBelongToUser;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class FileProcessor {

    private final String CONTENT_MARKER = "===CONTENT===";

    private String s1 = "123";
    private String s2 = "234";
    private String s3 = "345";
    private String s4 = "456";

    private String salt = (s2 + s1 + s4 + s3 + s3 + s2) + "______SALT";

    public File openFileForUser(User user, String fileName) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        MessageDigest md5Digest = null;

        try {
            md5Digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String userNameHashString;

        while ((userNameHashString = bufferedReader.readLine()) != null) {
            try {
                userNameHashString = new String(Base64.decode(userNameHashString), "UTF-8");
                String currentUserNameHash = new String(md5Digest.digest((user.getUserName() + salt).getBytes()), "UTF8");
                if (userNameHashString.equals(currentUserNameHash)) {
                    return new File(fileName);
                }
            } catch (Exception ignored){};
        }

        throw new FileIsNotBelongToUser();
    }

    public String readFileContent(File file) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String res;
        while ((res = bufferedReader.readLine()) != null && res.length() != 0) {
            if (res.contains(CONTENT_MARKER)) {
                break;
            }
        }

        StringBuilder contentBuilder = new StringBuilder();

        String contentString;
        while ((contentString = bufferedReader.readLine()) != null && contentString.length() != 0) {
            contentBuilder.append(contentString);
        }

        return contentBuilder.toString();
    }

    public void saveFileForUsers(List<User> readAllowedUsers, List<User> writeAllowedUsers, String fileName, String fileContent) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName));
        MessageDigest md5Digest = null;

        bufferedWriter.write("META_DATA_READ" + "\n");
        bufferedWriter.flush();

        try {
            md5Digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        for (User user : readAllowedUsers) {
            String userNameHash = new String(md5Digest.digest((user.getUserName() + salt).getBytes()), "UTF8");
            userNameHash = Base64.encode(userNameHash.getBytes());
            bufferedWriter.write(userNameHash + "\n");
        }

        bufferedWriter.write("META_DATA_WRITE" + "\n");
        bufferedWriter.flush();

        for (User user : writeAllowedUsers) {
            String userNameHash = new String(md5Digest.digest((user.getUserName() + salt).getBytes()), "UTF8");
            userNameHash = Base64.encode(userNameHash.getBytes());
            bufferedWriter.write(userNameHash + "\n");
        }

        bufferedWriter.write(CONTENT_MARKER + "\n");
        bufferedWriter.write(fileContent);
        bufferedWriter.flush();
    }

}
