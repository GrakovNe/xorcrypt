package org.grakovne.xorcryptor.roles;

public enum UserRole {
    USER,
    ADMIN
}
