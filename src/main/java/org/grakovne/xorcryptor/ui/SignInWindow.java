package org.grakovne.xorcryptor.ui;

import javax.swing.*;
import java.awt.*;

public class SignInWindow extends JFrame {

    private JLabel loginLabel;
    private JLabel passwordLabel;

    private JTextField loginTextField;
    private JTextField passwordTextField;

    private JButton signInButton;
    private JButton signUpButton;

    public SignInWindow() throws HeadlessException {
        super("XOR CRYPT 0.01");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("can't apply native interface");
        }

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(300, 50, 290, 200);
        setLayout(null);

        loginLabel = new JLabel("Логин");
        loginLabel.setBounds(10, 10, 265, 20);
        add(loginLabel);

        loginTextField = new JTextField();
        loginTextField.setBounds(10, 30, 265, 20);
        add(loginTextField);

        passwordLabel = new JLabel("Пароль");
        passwordLabel.setBounds(10,60, 265,20);
        add(passwordLabel);

        passwordTextField = new JTextField();
        passwordTextField.setBounds(10, 80, 265, 20);
        add(passwordTextField);

        signInButton = new JButton("Войти");
        signInButton.setBounds(10, 120, 100, 20);
        add(signInButton);

        signUpButton = new JButton("Зарегистрироваться");
        signUpButton.setBounds(170, 120, 100, 20);
        add(signUpButton);

        setResizable(false);
    }

    public JTextField getLoginTextField() {
        return loginTextField;
    }

    public JTextField getPasswordTextField() {
        return passwordTextField;
    }

    public JButton getSignInButton() {
        return signInButton;
    }

    public JButton getSignUpButton() {
        return signUpButton;
    }
}
