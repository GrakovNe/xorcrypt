package org.grakovne.xorcryptor.ui;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class SignUpWindow extends JFrame {

    private JLabel loginLabel;
    private JLabel passwordLabel;

    private JTextField loginTextField;
    private JTextField passwordTextField;

    private JButton signUpButton;

    private JComboBox<String> roleComboBox;

    public SignUpWindow() throws HeadlessException {
        super("XOR CRYPT 0.01");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("can't apply native interface");
        }

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(300, 50, 290, 230);
        setLayout(null);

        loginLabel = new JLabel("Логин");
        loginLabel.setBounds(10, 10, 265, 20);
        add(loginLabel);

        loginTextField = new JTextField();
        loginTextField.setBounds(10, 30, 265, 20);
        add(loginTextField);

        passwordLabel = new JLabel("Пароль");
        passwordLabel.setBounds(10,60, 265,20);
        add(passwordLabel);

        passwordTextField = new JTextField();
        passwordTextField.setBounds(10, 80, 265, 20);
        add(passwordTextField);

        Vector<String> availableRoles = new Vector<>();
        availableRoles.add("Пользователь");
        availableRoles.add("Администратор");

        roleComboBox = new JComboBox<>(availableRoles);
        roleComboBox.setBounds(40, 120, 200, 20);
        add(roleComboBox);

        signUpButton = new JButton("Зарегистрироваться");
        signUpButton.setBounds(40, 160, 200, 20);
        add(signUpButton);

        setResizable(false);
    }

    public JTextField getLoginTextField() {
        return loginTextField;
    }

    public JTextField getPasswordTextField() {
        return passwordTextField;
    }

    public JButton getSignUpButton() {
        return signUpButton;
    }

    public JComboBox<String> getRoleComboBox() {
        return roleComboBox;
    }
}
