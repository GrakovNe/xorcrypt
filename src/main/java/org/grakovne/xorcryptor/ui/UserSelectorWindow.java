package org.grakovne.xorcryptor.ui;

import javax.swing.*;
import java.awt.*;

public class UserSelectorWindow extends JFrame {

    private JButton okButton;
    private JList<String> readUserList;
    private JList<String> writeUserList;

    public UserSelectorWindow() throws HeadlessException {
        super("XOR CRYPT 0.01");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("can't apply native interface");
        }

        setBounds(300, 50, 605, 500);
        setLayout(null);

        readUserList = new JList<>();
        readUserList.setBounds(10, 10, 575, 200);
        readUserList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        add(readUserList);

        writeUserList = new JList<>();
        writeUserList.setBounds(10, 220, 575, 200);
        writeUserList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        add(writeUserList);

        okButton = new JButton("Сохранить");
        okButton.setBounds(240, 435, 100, 20);
        add(okButton);

        setResizable(false);
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JList<String> getReadUserList() {
        return readUserList;
    }

    public JList<String> getWriteUserList() {
        return writeUserList;
    }
}
