package org.grakovne.xorcryptor.ui;

import org.apache.commons.io.FileUtils;
import org.grakovne.xorcryptor.controllers.UserSelectorWindowController;
import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.fileprocessor.FileProcessor;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static java.awt.Font.SANS_SERIF;

@SuppressWarnings("Since15")
public class MainWindow extends JFrame {

    private JButton cryptBtn;
    private JButton decryptBtn;
    private JTextField keyField;

    private JTextArea openTextArea;
    private JTextArea cryptTextArea;

    private JButton saveCryptTextBtn;
    private JButton saveDecryptTextBtn;

    private JButton loadCryptTextBtn;
    private JButton loadDecryptTextBtn;

    private JFileChooser fileChooser;

    public MainWindow() {
        super("XOR CRYPT 0.01");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("can't apply native interface");
        }

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(300, 50, 605, 620);
        setLayout(null);

        openTextArea = new JTextArea("Default Text");
        openTextArea.setBounds(10, 10, 270, 480);
        openTextArea.setLineWrap(true);
        openTextArea.setFont(new Font(SANS_SERIF, Font.PLAIN, 12));
        add(openTextArea);

        cryptTextArea = new JTextArea();
        cryptTextArea.setBounds(320, 10, 250, 480);
        cryptTextArea.setLineWrap(true);
        add(cryptTextArea);

        cryptBtn = new JButton("ENCODE TEXT");
        cryptBtn.setBounds(100, 560, 100, 20);
        cryptTextArea.setFont(new Font(SANS_SERIF, Font.PLAIN, 12));
        add(cryptBtn);

        decryptBtn = new JButton("DECODE TEXT");
        decryptBtn.setBounds(400, 560, 100, 20);
        add(decryptBtn);

        keyField = new JTextField();
        keyField.setBounds(15, 530, 570, 20);
        add(keyField);

        fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("TEXT FILES", "txt");
        fileChooser.setFileFilter(filter);

        loadDecryptTextBtn = new JButton("OPEN FILE");
        loadDecryptTextBtn.setBounds(15, 500, 100, 20);
        add(loadDecryptTextBtn);

        saveDecryptTextBtn = new JButton("SAVE FILE");
        saveDecryptTextBtn.setBounds(175, 500, 100, 20);
        add(saveDecryptTextBtn);

        loadCryptTextBtn = new JButton("OPEN FILE");
        loadCryptTextBtn.setBounds(325, 500, 100, 20);
        add(loadCryptTextBtn);

        saveCryptTextBtn = new JButton("SAVE FILE");
        saveCryptTextBtn.setBounds(465, 500, 100, 20);
        add(saveCryptTextBtn);

        setResizable(false);

    }

    public void setCryptBtnListener(ActionListener listener) {
        cryptBtn.addActionListener(listener);
    }

    public void setDecryptBtnListener(ActionListener listener) {
        decryptBtn.addActionListener(listener);
    }

    public void setSaveDecryptTextBtnListener(ActionListener listener) {
        saveDecryptTextBtn.addActionListener(listener);
    }

    public void setSaveCryptTextBtnListener(ActionListener listener) {
        saveCryptTextBtn.addActionListener(listener);
    }

    public void setLoadCryptTextBtnListener(ActionListener listener) {
        loadCryptTextBtn.addActionListener(listener);
    }

    public void setLoadDecryptTextBtnListener(ActionListener listener) {
        loadDecryptTextBtn.addActionListener(listener);
    }

    public String getChosenFileContent(User user) {
        FileProcessor fileProcessor = new FileProcessor();

        if (fileChooser.showOpenDialog(getParent()) != JFileChooser.APPROVE_OPTION) {
            return "";
        }

        File selectedFile = fileChooser.getSelectedFile();

        try {
            File userFile = fileProcessor.openFileForUser(user, selectedFile.getAbsolutePath());
            return fileProcessor.readFileContent(userFile);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Выбранный файл не принадлежит пользователю");
        }

        return "";
    }

    public void saveContentToChosenFile(User user, String content){
        if (fileChooser.showSaveDialog(getParent()) != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File file = fileChooser.getSelectedFile();
        UserSelectorWindowController userSelectorWindowController = new UserSelectorWindowController();
        userSelectorWindowController.saveFileForUserAction(user, file.getAbsolutePath(), content);
    }

    public String getKeyText() {
        return keyField.getText();
    }

    public String getOpenText() {
        return openTextArea.getText();
    }

    public String getCryptText() {
        return cryptTextArea.getText();
    }

    public void setOpenText(String openText) {
        openTextArea.setText(openText);
    }

    public void setCryptText(String cryptText) {
        cryptTextArea.setText(cryptText);
    }

    public JButton getDecryptBtn() {
        return decryptBtn;
    }

    public JTextArea getCryptTextArea() {
        return cryptTextArea;
    }

    public JButton getSaveCryptTextBtn() {
        return saveCryptTextBtn;
    }

    public JButton getSaveDecryptTextBtn() {
        return saveDecryptTextBtn;
    }
}
