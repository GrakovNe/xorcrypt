package org.grakovne.xorcryptor.repositories;

import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.exceptions.EntityNotFound;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class UserRepository extends BaseRepository {

    public User gerUserById(long id){
        Session session = getSession();
        User user = session.load(User.class, id);
        session.close();

        if (null == user){
            throw new EntityNotFound();
        }

        return user;
    }

    public User getUserByName(String userName){
            Session session = getSession();
            Query query = session.createQuery("from User where userName=?1");
            query.setParameter(1, userName);

            if (query.list().size() == 0){
                return null;
            }

            User client = (User) query.list().get(0);
            session.close();

            return client;
    }

    public List<User> getUsers(){
        Session session =  getSession();

        List<User> users = session.createQuery("from User").list();

        session.close();

        return users;
    }

    public User createUser(User user){
        Session session = getSession();

        openTransaction(session);
        session.save(user);
        commitChanges(session);

        session.close();

        return user;
    }
}
