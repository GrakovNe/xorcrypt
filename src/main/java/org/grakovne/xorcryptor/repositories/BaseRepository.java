package org.grakovne.xorcryptor.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class BaseRepository {
    public Session getSession() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        return sessionFactory.openSession();
    }

    public void openTransaction(Session session) {
        session.beginTransaction();
    }

    public void commitChanges(Session session) {
        session.getTransaction().commit();
    }

}
