package org.grakovne.xorcryptor.common;

public class RolesPostfixManager {
    private static final String USER_ROLE_POSTFIX = "JHG_SFGBYjf3345";
    private static final String ADMIN_ROLE_POSTFIX = "hgYUGtTT8*Ujbh";

    public static String getUserRolePostfix(){
        return USER_ROLE_POSTFIX;
    }

    public static String getAdminRolePostfix(){
        return ADMIN_ROLE_POSTFIX;
    }
}
