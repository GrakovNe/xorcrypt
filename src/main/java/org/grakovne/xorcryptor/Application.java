package org.grakovne.xorcryptor;

import org.grakovne.xorcryptor.controllers.SignInWindowController;
import org.grakovne.xorcryptor.controllers.MainWindowController;
import org.grakovne.xorcryptor.controllers.SignUpWindowController;
import org.grakovne.xorcryptor.controllers.UserSelectorWindowController;
import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.fileprocessor.FileProcessor;
import org.grakovne.xorcryptor.ui.SignUpWindow;
import org.grakovne.xorcryptor.ui.UserSelectorWindow;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Application {
    public static void main(String[] args) throws UnsupportedEncodingException {
        MainWindowController mainWindowController = new MainWindowController();
        SignInWindowController signInWindowController = new SignInWindowController();
        UserSelectorWindowController userSelectorWindowController = new UserSelectorWindowController();

        signInWindowController.showWindow();

        //userSelectorWindowController.showWindow();
    }
}
