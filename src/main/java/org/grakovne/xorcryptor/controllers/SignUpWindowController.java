package org.grakovne.xorcryptor.controllers;

import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.exceptions.EntityAlreadyExists;
import org.grakovne.xorcryptor.roles.UserRole;
import org.grakovne.xorcryptor.services.UserService;
import org.grakovne.xorcryptor.ui.SignUpWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SignUpWindowController {
    private final SignUpWindow signUpWindow;
    private final UserService userService;

    private MainWindowController mainWindowController;

    public SignUpWindowController() {
        signUpWindow = new SignUpWindow();
        userService = new UserService();

        mainWindowController = new MainWindowController();

        initWindow();
    }

    private void initWindow() {
        signUpWindow.getSignUpButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userName = signUpWindow.getLoginTextField().getText();
                String password = signUpWindow.getPasswordTextField().getText();

                UserRole userRole = null;
                if (signUpWindow.getRoleComboBox().getSelectedItem().equals("Пользователь")) {
                    userRole = UserRole.USER;
                }

                if (signUpWindow.getRoleComboBox().getSelectedItem().equals("Администратор")) {
                    userRole = UserRole.ADMIN;
                }

                try {
                    User resultUser = userService.signUpUser(userName, password, userRole);

                    if (null != resultUser) {
                        JOptionPane.showMessageDialog(null, "Регистрация успешна!\n" + "Имя пользователя: " + userName);

                        resultUser = userService.signInUser(userName, password);

                        switch (resultUser.getRole()) {
                            case USER:
                                mainWindowController.showWindowForUser(resultUser);
                                break;
                            case ADMIN:
                                mainWindowController.showWindowForAdmin(resultUser);
                                break;
                        }
                    }

                } catch (EntityAlreadyExists ex) {
                    JOptionPane.showMessageDialog(null, "Пользователь с таким именем существует!");
                }
            }
        });
    }

    public void showWindow() {
        signUpWindow.setVisible(true);
    }

    public void hideWindow() {
        signUpWindow.setVisible(false);
    }
}
