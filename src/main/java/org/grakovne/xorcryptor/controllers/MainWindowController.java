package org.grakovne.xorcryptor.controllers;

import org.grakovne.xorcryptor.cryptpocessor.Base64Wrapper;
import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.ui.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainWindowController {
    final MainWindow mainWindow;

    private User user;

    public MainWindowController() {
        this.mainWindow = new MainWindow();
        initWindow();
    }

    private void initWindow(){
        mainWindow.setCryptBtnListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Base64Wrapper base64Wrapper = new Base64Wrapper();
                String resultText = base64Wrapper.encodeText(mainWindow.getOpenText(), mainWindow.getKeyText());
                mainWindow.setCryptText(resultText);
                mainWindow.setOpenText("");
            }
        });

        mainWindow.setDecryptBtnListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Base64Wrapper base64Wrapper = new Base64Wrapper();
                String resultText = base64Wrapper.decodeText(mainWindow.getCryptText(), mainWindow.getKeyText());
                mainWindow.setOpenText(resultText);
                mainWindow.setCryptText("");
            }
        });

        mainWindow.setLoadDecryptTextBtnListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(user.toString());

                String fileString = mainWindow.getChosenFileContent(user);
                mainWindow.setOpenText(fileString);
            }
        });

        mainWindow.setLoadCryptTextBtnListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String fileString = mainWindow.getChosenFileContent(user);
                mainWindow.setCryptText(fileString);
            }
        });

        mainWindow.setSaveDecryptTextBtnListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = mainWindow.getOpenText();
                mainWindow.saveContentToChosenFile(user, text);
            }
        });

        mainWindow.setSaveCryptTextBtnListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = mainWindow.getCryptText();
                mainWindow.saveContentToChosenFile(user, text);
            }
        });
    }

    public void showWindowForUser(User user){
        this.user = user;

        mainWindow.getSaveCryptTextBtn().setEnabled(false);
        mainWindow.getSaveDecryptTextBtn().setEnabled(false);

        mainWindow.setVisible(true);
    }

    public void showWindowForAdmin(User user){
        this.user = user;

        mainWindow.getSaveCryptTextBtn().setEnabled(true);
        mainWindow.getSaveDecryptTextBtn().setEnabled(true);

        mainWindow.setVisible(true);
    }

    public void hideWindow(){
        mainWindow.setVisible(false);
    }
}
