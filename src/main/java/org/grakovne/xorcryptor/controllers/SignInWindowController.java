package org.grakovne.xorcryptor.controllers;

import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.exceptions.EntityNotFound;
import org.grakovne.xorcryptor.exceptions.IncorrectPassword;
import org.grakovne.xorcryptor.services.UserService;
import org.grakovne.xorcryptor.ui.SignInWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SignInWindowController {
    private final UserService userService;
    private final SignInWindow signInWindow;

    private final SignUpWindowController signUpWindowController;

    private MainWindowController mainWindowController;

    public SignInWindowController() {
        signInWindow = new SignInWindow();
        userService = new UserService();

        mainWindowController = new MainWindowController();
        signUpWindowController = new SignUpWindowController();

        initWindow();
    }

    private void initWindow() {
        signInWindow.getSignInButton().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String userLogin = signInWindow.getLoginTextField().getText();
                String userPassword = signInWindow.getPasswordTextField().getText();

                try {
                    User resultUser = userService.signInUser(userLogin, userPassword);

                    switch (resultUser.getRole()) {
                        case USER:
                            mainWindowController.showWindowForUser(resultUser);
                            break;
                        case ADMIN:
                            mainWindowController.showWindowForAdmin(resultUser);
                            break;
                    }

                } catch (EntityNotFound ex) {
                    JOptionPane.showMessageDialog(null, "Пользователь с таким именем не найден!");
                } catch (IncorrectPassword ex) {
                    JOptionPane.showMessageDialog(null, "Некорректный пароль!");
                }
            }
        });

        signInWindow.getSignUpButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signUpWindowController.showWindow();
                hideWindow();
            }
        });

    }

    public void showWindow() {
        signInWindow.setVisible(true);
    }

    public void hideWindow() {
        signInWindow.setVisible(false);
    }
}
