package org.grakovne.xorcryptor.controllers;

import org.grakovne.xorcryptor.entity.User;
import org.grakovne.xorcryptor.fileprocessor.FileProcessor;
import org.grakovne.xorcryptor.services.UserService;
import org.grakovne.xorcryptor.ui.UserSelectorWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserSelectorWindowController {
    final UserSelectorWindow userSelectorWindow;

    private UserService userService = new UserService();
    private FileProcessor fileProcessor = new FileProcessor();

    private User activeUser;
    private String activeFileName;
    private String activeContent;

    public UserSelectorWindowController() {
        this.userSelectorWindow = new UserSelectorWindow();
        initWindow();
    }

    private void initWindow() {
        List<String> userNames = getUserNames();
        String[] userNamesStringArray = new String[userNames.size()];
        userNamesStringArray = userNames.toArray(userNamesStringArray);
        userSelectorWindow.getReadUserList().setListData(userNamesStringArray);
        userSelectorWindow.getWriteUserList().setListData(userNamesStringArray);


        userSelectorWindow.getOkButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] selectedUserNamesRead = getSelectedUsersRead();
                List<User> selectedUsersRead = new ArrayList<>(selectedUserNamesRead.length);

                String[] selectedUserNamesWrite = getSelectedUsersWrite();
                List<User> selectedUsersWrite = new ArrayList<>(selectedUserNamesWrite.length);

                for (String userName: selectedUserNamesRead){
                    selectedUsersRead.add(userService.getUserByUserName(userName));
                }

                if (!selectedUsersRead.contains(activeUser)){
                    selectedUsersRead.add(activeUser);
                }

                for (String userName: selectedUserNamesWrite){
                    selectedUsersWrite.add(userService.getUserByUserName(userName));
                }

                if (!selectedUsersWrite.contains(activeUser)){
                    selectedUsersWrite.add(activeUser);
                }

                try {
                    fileProcessor.saveFileForUsers(selectedUsersRead, selectedUsersWrite, activeFileName, activeContent);
                } catch (IOException ex){
                    ex.printStackTrace();
                }

                hideWindow();
            }
        });
    }

    public void saveFileForUserAction(User user, String fileName, String content){
        activeUser = user;
        activeFileName = fileName;
        activeContent = content;

        showWindow();
    }

    public void showWindow() {
        userSelectorWindow.setVisible(true);
    }

    public void hideWindow(){
        userSelectorWindow.setVisible(false);
    }

    private String[] getSelectedUsersRead(){
        return userSelectorWindow.getReadUserList().getSelectedValuesList().toArray(new String[]{});
    }

    private String[] getSelectedUsersWrite(){
        return userSelectorWindow.getWriteUserList().getSelectedValuesList().toArray(new String[]{});
    }

    private List<String> getUserNames(){
        List<User> users = userService.getAll();

        List<String> result = new ArrayList<>(users.size());

        for (User user: users){
            result.add(user.getUserName());
        }

        return result;
    }
}
